DROP TABLE IF EXISTS dogs;
CREATE TABLE dogs (id INTEGER NOT NULL PRIMARY KEY, name VARCHAR(50) NOT NULL);
INSERT INTO dogs
(id, name) 
VALUES 
(1, 'Plinio'),
(2, 'Fernando'),
(3, 'Prisicila'),
(4, 'Patrick'),
(5, 'Rafael'),
(6, 'Rafaela'),
(7, 'Pedro'),
(8, 'Andrade');

DROP TABLE IF EXISTS cats;
CREATE TABLE cats (id INTEGER NOT NULL PRIMARY KEY, name VARCHAR(50) NOT NULL);
INSERT INTO cats
(id, name) 
VALUES 
(1, 'Plinio'),
(2, 'Fernando'),
(3, 'Prisicila'),
(4, 'Miguel'),
(5, 'Rafael'),
(6, 'Oliver'),
(7, 'Pedro'),
(8, 'Andrade');

SELECT DISTINCT name FROM
(
  SELECT name FROM dogs
  UNION
  select name FROM cats
) ORDER BY name;