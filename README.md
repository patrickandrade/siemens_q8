## Question 8 for Siemens Programming skills and PL/SQL

This repository holds code for the eighth question of Siemens Programming skills and PL/SQL.

The code consists of an SQL query that select all distinct pet names:

Information about pets is kept in two separate tables:

TABLE dogs
id INTEGER NOT NULL PRIMARY KEY,
name VARCHAR(50) NOT NULL

TABLE cats
id INTEGER NOT NULL PRIMARY KEY,
name VARCHAR(50) NOT NULL
